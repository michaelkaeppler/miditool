# miditool - Analyzing MIDI files

## Objective
The aim of this project is to provide a lightweight, portable tool for getting basic information out of MIDI files, including some statistics. It is based on [Python](www.python.org) and the [Mido](https://mido.readthedocs.io/en/latest/) package.

## Requirements
You need only the [Conda](https://docs.conda.io/en/latest/) package manager, it will install all required packages for you. I suggest to use a [Miniconda](https://docs.conda.io/en/latest/miniconda.html) installation, which comes with both Python and Conda.

## Installation

    git clone https://gitlab.com/michaelkaeppler/miditool.git
    cd miditool
    conda env create -f environment.yaml

## Running

    conda activate miditool
    python miditool.py info tests/input/st_martin.mid

## Usage

    ## Get information about file format, ticks per beat, tracks
    python miditool.py info tests/input/st_martin.mid
    ## Show total duration, too (may take a moment)
    python miditool.py --show-total-duration info tests/input/st_martin.mid
    ## Process many files and write results to csv
    python miditool.py --write-to-csv midi_file_info.csv info *.mid
    ## Dump all midi messages to stdout
    python miditool.py messages tests/input/st_martin.mid
    ## Show only the first five messages
    python miditool.py --msgcount 5 messages tests/input/st_martin.mid

import unittest
from mido import MidiFile


class MidoTest(unittest.TestCase):
    def test_read_midi_file_header(self):
        midi_file = MidiFile("input/st_martin.mid")
        self.assertEqual(midi_file.type, 1)
        self.assertEqual(len(midi_file.tracks), 5)
        self.assertEqual(midi_file.ticks_per_beat, 384)


if __name__ == '__main__':
    unittest.main()

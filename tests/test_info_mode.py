import unittest
from mido import MidiFile
from miditool import get_info


class InfoModeTest(unittest.TestCase):
    def setUp(self):
        self.midi_object = MidiFile("input/st_martin.mid")

    def test_get_info_without_duration(self):
        info_dict = get_info(self.midi_object, show_duration=False)
        self.assertEqual(info_dict['Format'], 1)
        self.assertEqual(info_dict['Ticks_per_beat'], 384)
        self.assertEqual(info_dict['Track_count'], 5)

        with self.assertRaises(KeyError):
            info_dict['Total_duration']

        track_details = info_dict['Track_details']
        self.assertEqual(track_details[0]['message_count'], 6)
        self.assertEqual(track_details[1]['message_count'], 34)
        self.assertEqual(track_details[2]['message_count'], 24)
        self.assertEqual(track_details[3]['message_count'], 16)
        self.assertEqual(track_details[4]['message_count'], 14)
        # TODO - use another test file with track names
        # self.assertEqual(track_details[0]['name'], "foo")
        track_names = info_dict['Track_names']
        track_names_reference = "0: '' (6), 1: '' (34), 2: '' (24), 3: '' (16), 4: '' (14)"
        self.assertEqual(track_names, track_names_reference)

    def test_get_info_duration(self):
        info_dict = get_info(self.midi_object, show_duration=True)
        self.assertEqual(info_dict['Format'], 1)
        self.assertEqual(info_dict['Ticks_per_beat'], 384)
        self.assertEqual(info_dict['Track_count'], 5)

        duration = round(info_dict['Total_duration'], 1)
        self.assertEqual(duration, 10.7)

        track_details = info_dict['Track_details']
        self.assertEqual(track_details[0]['message_count'], 6)
        self.assertEqual(track_details[1]['message_count'], 34)
        self.assertEqual(track_details[2]['message_count'], 24)
        self.assertEqual(track_details[3]['message_count'], 16)
        self.assertEqual(track_details[4]['message_count'], 14)
        # TODO - use another test file with track names
        # self.assertEqual(track_details[0]['name'], "foo")
        track_names = info_dict['Track_names']
        track_names_reference = "0: '' (6), 1: '' (34), 2: '' (24), 3: '' (16), 4: '' (14)"
        self.assertEqual(track_names, track_names_reference)


if __name__ == '__main__':
    unittest.main()

from glob import glob
import argparse
import csv
from itertools import chain
from mido import MidiFile
from tqdm import tqdm


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("action", choices=['info', 'messages', 'stats'], help="action to perform on the midi file")
    parser.add_argument("midifile", help="path to midi file(s) that should be analyzed")
    parser.add_argument("--msgcount", type=int, help="(for action 'messages') number of messages to print out")
    parser.add_argument("--show-total-duration", action="store_true", help="(for action 'info') show total duration")
    parser.add_argument("--write-to-csv", metavar="CSVFILE", help="output results to specified csv file")
    return parser.parse_args()


def get_info(midi_object, show_duration):
    info_dict = {}
    info_dict['Format'] = midi_object.type
    midi_tracks = midi_object.tracks
    info_dict['Ticks_per_beat'] = midi_object.ticks_per_beat
    # The MIDI parser has to sum up all delta times to compute the total duration,
    # which is pretty expensive. Thus do not show it by default.
    if show_duration:
        print("Calculate total duration...")
        midi_total_length = midi_object.length
        info_dict['Total_duration'] = midi_total_length
    track_count = len(midi_tracks)
    info_dict['Track_count'] = track_count
    info_dict['Track_details'] = {}
    for nr, track in enumerate(midi_tracks):
        info_dict['Track_details'][nr] = {'name': track.name, 'message_count': len(track)}
    all_track_details = info_dict['Track_details'].items()
    # Format and store this for CSV output
    all_track_details_pretty = ["{}: '{}' ({})".format(nr, details['name'], details['message_count']) for nr, details in
                                all_track_details]
    info_dict['Track_names'] = ", ".join(all_track_details_pretty)
    return info_dict


def print_info(info_dict):
    MIDI_FORMAT_NAMES = ['single track', 'synchronous', 'asynchronous']
    print("MIDI file format: {} ({})".format(info_dict['Format'], MIDI_FORMAT_NAMES[info_dict['Format']]))
    if 'Total_duration' in info_dict:
        print("Total duration: {:.1f} s".format(info_dict['Total_duration']))
    print("Ticks per beat: {}".format(info_dict['Ticks_per_beat']))
    print("Number of tracks: {}".format(info_dict['Track_count']))
    print("Track list:")
    for nr, details in info_dict['Track_details'].items():
        print("Number {} - name: '{}', message count: {}".format(nr, details['name'], details['message_count']))


def print_messages(midi_object, message_count):
    if message_count:
        print("Showing first {} messages from each track now.".format(message_count))

    for i, track in enumerate(midi_object.tracks):
        print("Track {} - '{}':".format(i, track.name))
        if message_count:
            messages = track[:message_count]
        else:
            messages = track

        for msg in messages:
            print(msg)

        if message_count:
            remaining_message_count = len(track) - message_count
            if remaining_message_count < 0:
                remaining_message_count = 0
            print("(further {} messages follow)".format(remaining_message_count))


def print_statistics(midi_object):
    def print_message_types(message_types, message_count_total):
        message_types_sorted = sorted(message_types.items(), key=lambda item: item[1], reverse=True)
        for msg in message_types_sorted:
            message_type = msg[0]
            message_count = msg[1]
            print("'{}': {} ({:.1%})".format(message_type, message_count, message_count / message_count_total))

    tracks = midi_object.tracks
    message_types_per_track = [['Meta: ' + msg.type if msg.is_meta else msg.type for msg in track] for track in tracks]
    # Flatten the list
    all_message_types = list(chain(*message_types_per_track))
    unique_message_types = set(all_message_types)
    message_types_count_per_track = [
        {msg_type: track.count(msg_type) for msg_type in unique_message_types if track.count(msg_type) > 0} for track in
        message_types_per_track]

    print("Message types per track:")
    for i, track in enumerate(message_types_count_per_track):
        print("--- Track {} ---".format(i))
        message_count_total = sum(track.values())
        print_message_types(track, message_count_total)

    print("Message types in total:")
    message_types_count_all_tracks = {msg_type: all_message_types.count(msg_type) for msg_type in unique_message_types}
    message_count_total = len(all_message_types)
    print_message_types(message_types_count_all_tracks, message_count_total)


def main():
    args = parse_arguments()
    midi_file_paths = glob(args.midifile)

    csv_fields = ["Filename", "Error", "Format", "Total_duration", "Ticks_per_beat", "Track_count", "Track_names"]

    if args.write_to_csv:
        csv_file = open(args.write_to_csv, 'w', encoding='utf-8', newline='')
        csv_writer = csv.DictWriter(csv_file, fieldnames=csv_fields, extrasaction='ignore', delimiter=';')
        csv_writer.writeheader()
        # Show progress bar
        midi_file_paths = tqdm(midi_file_paths)
    else:
        csv_writer = None

    for path in midi_file_paths:
        parsing_error = ''
        csv_row = {}
        try:
            read_message = "Reading file {}...".format(path)
            if csv_writer:
                tqdm.write(read_message)
            else:
                print(read_message)
            midi_object = MidiFile(path)
        except Exception as e:
            parsing_error = repr(e)
            print("\nError: " + parsing_error)
            midi_object = None

        csv_row['Filename'] = path
        csv_row['Error'] = parsing_error

        if midi_object:
            if args.action == "info":
                info_dict = get_info(midi_object, args.show_total_duration)
                # Do not clutter console when outputting csv
                if csv_writer:
                    csv_row.update(info_dict)
                else:
                    print_info(info_dict)
            elif args.action == "messages":
                print_messages(midi_object, args.msgcount)
            elif args.action == "stats":
                print_statistics(midi_object)

        if csv_writer:
            csv_writer.writerow(csv_row)

    if csv_writer:
        csv_file.close()


if __name__ == '__main__':
    main()
